<!DOCTYPE HTML>
<html lang="zxx">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Antarin</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/slicknav.min.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/icofont.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/slick.css')}}">
	<link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.carousel.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/magnific-popup.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/switcher-style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/fontawesome/css/all.css')}}" media="all" />
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}" media="all" />
	<link rel="icon" type="image/png" href="{{asset('assets/img/favcion.png')}}" />
</head>
@yield('extrahead')
<body data-spy="scroll" data-target=".header" data-offset="50">
	<div id="preloader"></div>
	<header class="header">
		<div class="container">
			<div class="row flexbox-center">
				<div class="col-lg-2 col-md-3 col-6">
					<div class="logo">
						<a href="#home"><img src="assets/img/logo.png" alt="logo" /></a>
					</div>
				</div>
				<div class="col-lg-10 col-md-9 col-6">
					<div class="responsive-menu"></div>
					<div class="mainmenu">
                        <ul id="primary-menu">
                            <li><a class="nav-link active" href="{{route('user.home')}}">Home</a></li>
                            <li><a class="nav-link" href="{{route('travel.index')}}">Travel</a></li>
                            <li><a class="nav-link" href="{{route('pesan.index')}}">Pesan Travel</a></li>
                            <li><a class="nav-link" href="">Pesan</a></li>
                        	<li><a class="appao-btn" href="#">Login / Daftar</a></li>
                    	</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
	@yield('content')
	<footer class="footer" id="contact">
		<div class="container">
			<div class="row">
            	<div class="col-lg-6">
					<div class="contact-form">
						<h4>Get in Touch</h4>
						<p class="form-message"></p>
						<form id="contact-form" action="#" method="POST">
				        	<input type="text" name="name" placeholder="Enter Your Name">
				            <input type="email" name="email" placeholder="Enter Your Email">
				            <input type="text" name="subject" placeholder="Your Subject">
				            <textarea placeholder="Messege" name="message"></textarea>
				            <button type="submit" name="submit">Send Message</button>
				        </form>
					</div>
                </div>
                <div class="col-lg-6">
					<div class="contact-address">
						<h4>Address</h4>
						<p>Jl. Udang Windu 60, Kabupaten Jember</p>
						<ul>
							<li>
								<div class="contact-address-icon">
									<i class="icofont icofont-headphone-alt"></i>
								</div>
								<div class="contact-address-info">
									<a href="callto:#">+6281259902612</a>
									<a href="callto:#">+6289623834328</a>
								</div>
							</li>
							<li>
								<div class="contact-address-icon">
									<i class="icofont icofont-envelope"></i>
								</div>
								<div class="contact-address-info">
									<a href="mailto:antar.instartup@gmail.com">antar.instartup@gmail.com</a>
								</div>
							</li>
							<li>
								<div class="contact-address-icon">
									<i class="icofont icofont-web"></i>
								</div>
								<div class="contact-address-info">
									<a href="www.antarin.com">www.antarin.com</a>
								</div>
							</li>
						</ul>
					</div>
                </div>
			</div>
			<div class="row">
                <div class="col-lg-12">
					<div class="copyright-area">
						<ul>
							<li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li><a href="#"><i class="fab fa-instagram"></i></a></li>
							<li><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>
                </div>
			</div>
		</div>
	</footer>
	<a href="#" class="scrollToTop">
		<i class="icofont icofont-arrow-up"></i>
	</a>
	<script src="{{asset('assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.slicknav.min.js')}}"></script>
	<script src="{{asset('assets/js/slick.min.js')}}"></script>
	<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
	<script src="{{asset('assets/js/waypoints.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.mb.YTPlayer.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.easing.1.3.js')}}"></script>
	<script src="{{asset('assets/js/gmap3.min.js')}}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBnKyOpsNq-vWYtrwayN3BkF3b4k3O9A_A"></script>
	<script src="{{asset('assets/js/custom-map.js')}}"></script>
	<script src="{{asset('assets/js/wow-1.3.0.min.js')}}"></script>
	<script src="{{asset('assets/js/switcher.js')}}"></script>
	<script src="{{asset('assets/js/main.js')}}"></script>
</body>
</html>